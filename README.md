# Wilson S. Anjos

<img src="https://i.imgur.com/Tp61ulN.jpg?width=200" />

Local: João Pessoa - PB | Idade: 26 anos | Formado em Sistema para Internet - Unipê

Sou desenvolvedor web, amo o que faço, gosto muito de aprender novas tecnologias.
Adoro encara novas desafios e ajudar, nas hora vagas amo jogar, fico sempre curioso pra saber como a mecanica de jogo funciona sempre busco aprender mais sobre e desenvolver.

Atualmente busco ter uma oportunidade de aprender mais.

Primeiro desafio ao sair da faculdade era problema que meus amigos tinham dentro jogo, registros de dados dos jogadores que passava no hospital os mesmo era tudo feito via **discord** dados como prontuarios, plano de saude, blacklist. Então pensei desenvolver plataforma web para resolver que foi primeiro sistema utilizando framework em codeigniter atualtamente novo desafio é coloca tudo em laravel. 

Projeto: [http://webtell.com.br/lsems](http://webtell.com.br/lsems "Projeto pessoal")   
gitlab: [https://gitlab.com/maxwilson](https://gitlab.com/maxwilson "pessoal")  
Email: wilsonanjos_@live.com

## Conhecimentos

* PHP (3 anos)
* Laravel ( 7 meses )
* Java ( 3 meses )
* Spring Boot ( Básico )
* Python (1 ano)
* Javascript (9 meses)
* HTML e CSS ( Básico )
* Node.Js (2 meses )
* ReactJS ( 2 meses )
* React Native ( 2 meses )
* Mysql, MongoDB, Sqlite
* Sistemas de controle de versão (git)
* Automatização em Python, PHP ( coleta de dados na internet )

## Projetos

### Open Sources

* **Be The Hero** (ReactJS, React Native, Node) ([link](https://gitlab.com/maxwilson/be-the-hero "gitlab"))  
 
   Aplicativos de ongs. Curso da Rockeatseat. Adicionei alertas e validações.

* **WebHook do Discord** (PHP) ([link](https://github.com/Maxwilson01/webhook-discord "github"))  
 
   Tutorial ensinando como utilizar webhook do discord com php feito para youtube ([link](https://www.youtube.com/watch?v=Rme96yAtVCE&t "YouTube")).


* **Outros no gitlab** ([link](https://gitlab.com/maxwilson/ "link")) 


### Proprietários

* **Controle Hospital** ( Codeigniter PHP ) ([screenshot](https://gitlab.com/maxwilson/curriculo/-/tree/master/images/lsems "fotos"))  

  Controle de prontuario, plano de saude... Desenvolvido para jogo GTAV:RP melhor controle dos registros.

* **Siph** ( Laravel ) ([screenshot](https://gitlab.com/maxwilson/curriculo/-/tree/master/images/siph "foto"))  

  Sistema controle hospital feito em laravel com bastante melhorias... Desenvolvido para jogo GTAV:RP melhor controle dos registros. Em desenvolvimento.

## Formação

* Graduado em Sistema para internet pela (UNIPÊ)
* Pós em desenvolvimento web e mobile pela (UNIPÊ)

